package javagotchi.controller.minigame;

import javagotchi.controller.minigame.audio.Music;
import javagotchi.controller.minigame.audio.MusicImpl;
import javagotchi.controller.minigame.file.SavedData;
import javagotchi.controller.minigame.file.SavedDataImpl;

/**
 * 
 * @author marica
 *
 */
public class FactoryControllerImpl implements FactoryController {

    @Override
    public final ControllerMiniGame getControllerMiniGame() {
        return ControllerMiniGameImpl.getInstance();
    }

    @Override
    public final Music getMusic() {
        return MusicImpl.getInstance();
    }

    @Override
    public final SavedData getSavedData() {
        return SavedDataImpl.getInstance();
    }
}
