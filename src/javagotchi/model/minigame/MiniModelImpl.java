package javagotchi.model.minigame;

import java.util.Random;

/**
 * 
 * @author marica
 *
 */
public class MiniModelImpl implements MiniModel {

    private GameGrid gameGrid;
    private Time time;
    private Score score;
    private GameState gameState;

    /**
     * Constructor for the ModelImpl.
     */
    public MiniModelImpl() {
        initModel();
        gameState = GameState.NULL;
    }

    @Override
    public final GameGrid getGameGrid() {
        return gameGrid;
    }

    @Override
    public final Time getTime() {
        return time;
    }

    @Override
    public final void setTime(final Integer sec) {
        if (sec == null) {
            this.time = this.time.restart();
        } else {
            this.time = new TimeImpl(sec);
        }
    }

    @Override
    public final Score getScore() {
        return score;
    }

    @Override
    public final void setScore(final Integer score) {
        this.score = new ScoreImpl(score);
    }

    @Override
    public final void setGameState(final GameState state) {
        gameState = state;
    }

    @Override
    public final boolean isGameState(final GameState state) {
        return gameState.equals(state);
    }

    @Override
    public final void initModel() {
        this.score = new ScoreImpl();
        this.gameGrid = new GameGridImpl();
        this.time = new TimeImpl();
    }

    @Override
    public final boolean isMomentToAddTime() {
        return new Random().nextInt(this.time.getStartTime() - 1) + 1 == this.time.getSeconds();
    }

    @Override
    public final String toString() {
        return "GameState: " + gameState + ", Time: " + time.getSeconds() + ", Score : " + score.getCurrentScore();
    }

}
