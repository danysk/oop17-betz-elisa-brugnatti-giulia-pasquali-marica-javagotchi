package javagotchi.view.menu;
/**
 * this is an interface that models a generic menu.
 * @author giulia
 *
 */
public interface Menu {
    /**
     * This is the method that shows the implemented menu.
     */
    void show();
}
