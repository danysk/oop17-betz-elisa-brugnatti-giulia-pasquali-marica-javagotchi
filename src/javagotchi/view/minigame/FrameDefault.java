package javagotchi.view.minigame;

/**
 * 
 * @author maric
 *
 */
public interface FrameDefault {

    /**
     * Shows this Window.
     */
    void display();

    /**
     * Hides this Window.
     */
    void hideWindow();

    /**
     * {@link JFrame#dispose}.
     */
    void disposeWindow();

    /**
     * Sets event of buttons.
     */
    void setEvent();
}
